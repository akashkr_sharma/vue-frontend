import {apis, mailer} from './apis'
export function getAllCatagories () {
  let method = 'get'
  let url = 'auth/category'
  let data = apis(method, url)
  return data
}

export function getAllServices () {
  let method = 'get'
  let url = 'auth/services'
  let data = apis(method, url)
  return data
}

export function getAllPhotograpr () {
  let method = 'get'
  let url = 'auth/photograpr'
  let data = apis(method, url)
  return data
}

export function getAllImages () {
  let catagory = 'thanksgiving'
  let action = 'display'
  let data = mailer(catagory, action)
  return data
}

export function login (data) {
  let method = 'post'
  let url = 'auth/login/'
  let responseData = apis(method, url, data)
  // console.log('login: ', responseData)
  return responseData
}

export function register (data) {
  let method = 'post'
  let url = 'auth/register/'
  let responseData = apis(method, url, data)
  return responseData
}

export default {
  getAllCatagories: getAllCatagories,
  getAllServices: getAllServices,
  getAllPhotograpr: getAllPhotograpr,
  getAllImages: getAllImages
}
