import axios from 'axios'
import {route, mialerRoute} from './route.config'
export async function apis (method, url, data) {
  var promise = ''
  try {
    promise = await axios({
      method: method,
      url: route().APIUrl + url,
      data: data,
      withCredentials: false,
      responseType: 'json',
      headers: {
        'content-type': 'application/json',
        'Accept': '*/*'
        // 'Access-Control-Request-Headers': 'accesstoken, content-type'
      }
    }).then((success) => {
      return success
    }).catch((error) => {
      return error
    })
  } catch (exce) {
    return exce
  }
  return promise
}

export async function mailer (catagory, action, optional) {
  var promise = ''
  let actionUrl = []
  let mailerUrl = mialerRoute()[catagory][action]
  if (action !== 'download') {
    actionUrl = mailerUrl.link
  } else if (action === 'download' && typeof (optional) === 'object') {
    actionUrl = mailerUrl.link + optional
  } else {
    return 'Please add link in object'
  }
  try {
    promise = await axios({
      method: mailerUrl.method,
      url: actionUrl
    }).then((success) => {
      return success
    }).catch((error) => {
      return error
    })
  } catch (exce) {
    return exce
  }
  return promise
}

export default {
  apis: apis,
  mailer: mailer
}
