export const route = () => {
  let getLocationDomain = location.hostname.split('.').reverse()[0]
  let setting = {
    local: {
      apiUrl: 'http://django.photograpr.dev/',
      uiUrl: 'http://photograpr.dev/'
    },
    server: {
      apiUrl: 'http://139.59.71.164:8000/',
      uiUrl: 'http://139.59.71.164/'
    }
  }
  let APIUrl = ''
  let UIUrl = ''
  if (getLocationDomain === 'dev' || location.hostname === '127.0.0.1' || location.hostname === 'localhost') {
    APIUrl = setting.local.apiUrl
    UIUrl = setting.local.uiUrl
  } else {
    APIUrl = setting.server.apiUrl
    UIUrl = setting.server.uiUrl
  }
  return {
    APIUrl: APIUrl,
    UIUrl: UIUrl
  }
}

export const mialerRoute = () => {
  let awsSelfLinkRoute = 'http://139.59.71.164:8002/upload/links?category='
  let awsDownloadRoute = 'http://139.59.71.164:8002/upload/links?category='
  return {
    thanksgiving: {
      display: {
        link: awsSelfLinkRoute + 'thanksgiving',
        method: 'get'
      },
      download: {
        link: awsDownloadRoute,
        method: 'get'
      }
    }
  }
}
