module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    title: 'photograpr.com',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'book photographers on-demand in your area' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '~/assets/images/favicon.ico' },
      { rel: 'stylesheet', href: 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css'},
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/icon?family=Material+Icons'},
      { rel: 'script', href: 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js'},
      { rel: 'script', href: 'https://code.jquery.com/jquery-3.2.1.slim.min.js'},
      { rel: 'script', href: 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js'},
      { rel: 'script', href: 'https://getbootstrap.com/assets/js/vendor/holder.min.js'},
      { rel: 'script', href: 'https://getbootstrap.com/assets/js/vendor/popper.min.js'}
    ]
  },
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#3B8070' },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** Run ESLint on save
    */
    extend (config, ctx) {
      if (ctx.dev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  },

  css: [
    // CSS file in the project
    '@/assets/css/carousel.css',
    '@/assets/css/auth.css'
  ],
  router: {
    middleware: 'checkAuth'
  }
}
